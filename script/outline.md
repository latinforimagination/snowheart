# Snowheart

## Cover

## Cover Back

## Dedication

_Hazel and Ruby building a Snowman_

**For Hazel and Ruby**
who are stronger together than they are alone

## Page 1

_A snowy path stretches down the hill into the woods, spanning two pages_

Just a few blocks down the street there is a big, big hill with a trail that squiggles all the way down like a twisty turny rope-

until it disappears into a cluster of bramble patches surrounding a deep, dark forest.

## Page 2

_Continuation of previous illustration, transitioning from hill to forest_

## Page 3

_"Happy" forest. This should (lightly) transition from the previous page turn, and seamlessly transition onto the next page_

Nowadays, the forest is teeming with animals that come from all over the county just to visit. But it wasn't always this way.

## Page 4

_Continuation of previous illustration with forest in its darker state._

Once, the forest was quiet and still - except for on very cold nights when you could hear something like the wind, howling through the trees.

This is a story from that time.

## Page 5

_Full page: Hazel and Ruby talking over bunk bed_

When Hazel and Ruby woke up it had already been snowing for a good long while, and the whole world glittered and blinded them in greeting when they looked outside the window.

Hazel grinned.

"Today, Ruby, we're going to the park!

We'll pack supplies so we can go sledding and make a snow fort and see the animal tracks!"

## Page 6

_Full page: Hazel and Ruby sitting at the table_

At the table, they found four pieces of toast and a big plate of scrambled eggs.

Hazel took stock.

_Two popouts: Division of toast and eggs arranged on two plates_

"We can each have two pieces of toast, but anyone can take as much of the eggs as they want."

## Page 7

_Popout: Ruby stares at Hazel with a blank expression_

_Popout: Ruby takes the entire plate of toast_

## Page 8

_TODO: description_

"No!", Hazel reprimanded. "Weren't you listening Ruby? You didn't even take any eggs!"

"But I don't like eggs!" complained Ruby.

"But there's a huge plate of eggs, and there's exactly five pieces of toast so it doesn't make sense to - "

Ruby ran upstairs before Hazel could finish.

## Page 9

_Background: Hazel putting things into a backpack_

_Popout: Yellow scarf_

Back upstairs, Hazel pulled out an old scratchy backpack. She put in a yellow scarf with frills,

_Popout: Two apples_

two apples in case she or Ruby got hungry,

_Popout: An extra pair of gloves_

and an extra pair of purple gloves.

_Popout: A pair of purple glasses with stars on them_

Ruby packed a pair of sunglasses.

## Page 10

_Background: Hazel with a blank face_

Hazel didn't say anything.

But she did think it.

Just a little bit.

## Page 11

_Background: the two children looking down at the trail_

When they got to the park, Ruby and Hazel dragged their sled over to the edge and stood at the top of the big, big hill looking down.

The trail wandered back and forth as if it couldn't quite make up its mind which direction was the best way to go. At the bottom lay a muddy brier patch surrounding a thick grove of trees. The path twisted sharply to the right as if in a sudden fit of resolution. Wherever it did end up, it seemed it wouldn't be within those woods.

## Page 12

_Background: seamless continuation of the last page_

It was impossible for the two children not to feel a sense of exhilaration as their eyes traced out the sledding route.


"We'll go down together, " Hazel said. "You hang on to me tightly, and then when we get close to the bottom, we both need to lean to the right as hard as we can!"

_Popout: Ruby nods_

## Page 13

_TODO: Backgrounds below here_

The two children started off slowly (the fluffy snow wasn't quite packed enough to really accelerate), but as they picked up momentum they sped up and up until the world was a blur around them.

"Now Ruby!" commanded Hazel, and the two children leaned right as hard as they could. The sled banked sharply and whipped around the corner like a bullet, moving along the path into a wide open field and eventually slowing to a stop on the level ground.

## Page 14

The girls looked at each other, half breathless and half giggles.

"We have got to do that again," said Hazel.

Hazel and Ruby trudged up the path back towards the top of the hill, pausing to catch their breaths near the brier patch. (timing is weird, brier patch is at the bottom)

## Page 15

Ruby stared into the forest. "It's scary", she remarked.

"I heard at school that there's a Medusa in there."

"What's a Medusa?" Ruby asked.

## Page 16

_Background: Ruby confronted with dark visualization of what Medusa beast might look like_

"A Medusa is a creature that lives inside woods like this, " Hazel explained. "If you ever look it in the eyes, you turn into stone!"

## Page 17

Ruby scrunched up her face. "That isn't real!"

"Of course it's real, I already said that I learned it at school!" Hazel corrected. "If a Medusa ever shows up, you have to close your eyes. But then you can't see, and that's when the Medusa **gets you!**"

"No, it's not real!" Ruby protested.

## Page 18

"Stop being difficult!"

_Background: Ruby clenches her fists, looks angry. It's obvious that Hazel has hit a nerve_

## Page 19

The two children climbed back onto the sled and pushed off. The trail was tightly packed now, and the sled picked up speed quickly.

Ruby felt the wind and snow whipping past, drowning out the world around her. The sled moved faster and faster, blurring into an indecipherable blob of white and blue.

## Page 20

"Now, Ruby!" Hazel yelled, "lean!"

But Ruby didn't lean.

## Page 21

The sled flew off of the trail and careened towards the brier patch. In an instant of panic Hazel grabbed at Ruby and the two of them tumbled over and over into the snow. They lay quietly for a few moments.

## Page 22

Hazel carefully got up and checked to make sure she wasn't hurt, then turned on Ruby. "Weren't you listening?" she said, angrily. 

"I **told** you we had to lean at the same time, and I **told** you that if we didn't lean we wouldn't make the turn and then we'd go straight into the brier patch and get-"

"My hands are cold", Ruby interjected quietly. She was still sitting down in the snow, looking at her now reddened fingers with an unreadable expression.

## Page 23

Hazel sighed and reached into her bag.

"I knew I needed to bring an extra pair of gloves because you didn't pack **any**."

Ruby didn't say anything. But she did take the gloves.

## Page 24

Hazel turned to trumble back up the hill when suddenly she noticed something missing.

"Ruby, where's the sled?" she asked, her eyes already widening with realization.

*Ruby points into the forest*

## Page 25

_2 page spread_

The woods swallowed the two children up.

"OK Ruby, we're just going to grab the sled, and then we're going to turn right around."

Ruby plowed ahead, but Hazel grabbed her hand and pulled her back.

"The first thing to remember when you're in the woods is to not split up."

## Page 26

Hazel pointed through the trees. "That's the direction we came. Make sure you remember it. That way, even if I get turned around, between the two of us we'll be able to figure it out."

The two plowed on.

## Page 27

"It's really quiet." Hazel remarked nervously. "Should woods be this quiet?"

Ruby didn't respond. The two moved deeper into the trees.

"I thought the sled would have gone more to the right," Hazel remarked nervously. "I don't know if we should go any farther than this."

## Page 28

Ruby wrestled her hand out of Hazel's and poked her head into a bush to see if anything was hiding behind it.

"This was a bad idea. I'm sure it didn't come this deep. Maybe it got caught under the brambles and we just walked past it."

"Hazel, look!"

## Page 29

There was a strange, dark shape behind the bush. Hazel and Ruby peered forward, squinting until they could make it out in the shadows. It was a badger, sitting silently, unmoving - like no animal they had ever seen before.

It was made of stone.

## Page 30

Hazel acted. "Ruby, close your eyes **right now**!"

## Page 31

_Background: pure black_

## Page 32

_Background: pure black_

## Page 33

Hazel gripped Ruby's arm tightly and stumbled through the forest, tripping over rocks and branches.

"Do you remember which direction we came?"

Ruby shook her head, then remembered that Hazel couldn't see her. "No!"

A wave of doubt rushed over Hazel, but she shook it aside and kept her voice calm.

"It's OK Ruby. I am pretty sure I do. We just need to-"

## Page 34

Beside them, a branch snapped. Something was moving. It was too big to be a squirrel or a bird.

It was moving closer.

"You shouldn't be here", the something said.

## Page 35

Hazel pulled Ruby away, half sprinting through the thick underbrush.

"Wait!" The voice called behind them, frantically. Hazel could here it running behind them. It was keeping pace.

Her lungs felt like they were about to burst, but she was certain that this was the right direction. If she and Ruby could just move a little bit faster, in just a few more seconds they'd find themselves wading into soft snow, safe and sound, and everything would be-

## Page 36

Hazel tripped over something hard. It was covered with odd, unnatural lumps and sharp corners. Her hand felt around the cold surface, and with horror she realized that it was a stone badger.

They had been running in circles.

## Page 37

Hazel wondered what it would be like to be made of stone. Would she be stuck here, forever?

Would she be able to go to school? Hazel imagined herself sitting alone, in the woods, for days and weeks and months, waiting for something to happen. She clutched Ruby's hand tighter.

But Ruby's hand wasn't there.

## Page 38

You see, Ruby had been thinking too, about everything that Hazel had told her - about everything she knew.

She thought about that morning, and the way she felt when she was packing. She thought about whooshing down the hill, and the strange, almost purposeful way the path curved sharply to the right. She thought about the argument that she and Hazel had shared, and about a creature that could turn you to stone. She thought about a badger, running in circles, until it finally opened up its eyes in desperation. And she thought very hard about exactly where that badger was. And where she was.

And suddenly, something clicked. And Ruby decided to do something very brave.

## Page 39

She slipped her hand away from Hazel's and blindly felt along the snowy ground-

-until she felt the rough cloth surface of her bag

## Page 40

-and felt within the bag until her fingers closed around a small, solid object

-and stumbled across the snowy ground until she bumped into a strange, warm object

-and reached up until she felt its face

*Ruby puts the glasses on Medusa*

## Page 41

"Oh", said the Medusa.

Ruby opened her eyes. "Hello", she said.

Ruby took stock.

*Medusa: A girl, roughly the same age as Hazel. Her hair is made of snakes but tightly woven into braids, which makes the effect a lot less frightening.
Not much taller than Ruby - her size isn't intimidating but her build is. Living alone in woods for her entire life has left her more muscular than would be natural for her age.*

*TODO: dressed in fur or leaves? Medusa wouldn't have killed an animal to take its fur - likely she found the fur discarded by a different predator.
Medusa is a gatherer/scavenger, not a hunter - no animal would ever get close enough to her to hunt, and if they did they'd just turn to stone anyway.
Maybe distracting to her character, maybe adds depth - would need to see people's reactions to know for sure. Also potentially less vegetarian friendly.*

The creature stood awkwardly, like she wasn't sure what to do or how to respond. Hazel ran over, shaking a little, and grabbed Ruby's arm.

"I'm Ruby, and this is Hazel." Ruby tried again, holding out her hand.

Hesitantly, the Medusa reached out and shook her hand.

## Page 42

"You're a Medusa, right?"

"A gorgon, actually," the creature replied sheepishly. "Medusa's just my name."

"That's a weird name," Ruby offered cheerfully.

"Don't be rude, Ruby!" Hazel reprimanded. "Do you live in here?"

*TODO: should Medusa expand on this? Wait and see if you need more pages.*

"Yeah. For as long as I can remember."

## Page 43

"I saw where your sled went," Medusa pointed. "That's why you're in here, right?"

Ruby and Hazel nodded.

"Nobody ever comes in here anymore. None of the animals do either, because whenever I try to play with them I turn them into stone."

She looked around miserably at the forest around her, littered with statues. "I can tell that they all hate me."

## Page 44

Hazel's instincts took over. "Actually, all you have to do is wear those sunglasses. You won't turn anyone into stone then. And anybody can come into the forest and play with you!"

"I can keep them?" Medusa beamed.

"Of course!" She paused, "I mean, I guess they're technically Ruby's. But she can have them, right Ruby?"

Ruby nodded.

## Page 45

They reached the edge of the forest and the gorgan stopped. Hazel wondered if she'd ever been out this far before.

## Page 46

A fox ran past and the Medusa instinctively raised a hand in front of her face, then cautiously lowered it. She waved to the fox.

"Thank you."

## Page 47

The day was over, and the setting sun cast a deep purple shadow over the once-white blanket of snow.

## Page 48

As the first of the stars began to twinkle into existence, the two girls climbed into bed, both thinking about their adventures.

*Do Ruby and Hazel have bunk beds? I don't remember. Does it matter?*

## Page 49

After a while, Hazel spoke.

"Hey, Ruby? I'm glad you brought the sunglasses. And I'm glad that you were there when we got lost, you were really clever. I didn't know what to do."

"Thank you for the gloves, and for taking care of me in the forest. I'm sorry I wrecked the sled."

"It's OK, Ruby. I mean, it turned out OK, right?"

The children sat in silence for a few minutes.

## Page 50

"Ruby?"

"Yeah?"

## Page 51

"If I was turned into stone, you'd come visit me right? I mean... I was thinking about the animals, and if you hadn't brought the sunglasses, or if I had closed my eyes just a second later, or..." she trailed off.

Ruby flipped herself out of bed, flipped on a lamp, and walked over to Hazel's stack of books. She pulled down one that Hazel hadn't finished yet.

## Page 52

And Hazel laughed. "That's a great idea! We can go down tomorrow and we can read to them! And we can get their other animal friends to visit too since now the Medusa won't turn them into stone! And none of them will have to be lonely!"

## Page 53

Ruby climbed back into bed and slipped the book beneath her pillow, and the two children closed their eyes. The snow outside the house drifted down lazily, blanketing the world once again.

Now, of course, it was too far away for the children to really, honestly make out-

## Page 54

-but as they drifted off to sleep, both of them, just for a second, could swear that they heard an owl hooting from within a deep, dark forest.

## Page 55 (end page)

*Hazel reading to the stone animals, Medusa, and Ruby*
