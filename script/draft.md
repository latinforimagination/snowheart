# Snowheart

## Introduction

### Page 1 and 2 [x]

Just a few blocks ~~from your house you'll find~~ down the street there is a big, big hill with a trail that squiggles all the way down like a twisty turny rope-

until it disappears into a cluster of bramble patches and a deep, dark forest.

### Page 3 [x]

Nowadays, the forest is teeming with animals that come from all over the county, just to visit. But it wasn't always this way.

### Page 4 [x]

Once, the forest was quiet and still - except for on very cold nights when you could hear something like the wind, howling through the trees.

This is a story from that time.

## A Winter Morning

### Page 5 [x]

When Hazel and Ruby woke up it had already been snowing for a good long while, and the whole world glittered and blinded them in greeting when they looked outside the window.

Hazel grinned. "Today, Ruby, we're going to the park. We'll pack supplies so we can go sledding and make a snow fort and see the animal tracks!"

~~Ruby nodded, and the two ran downstairs for breakfast.~~ (shown, not written)

### At the Table

#### Page 6 [X]

At the table, they found 5 pieces of toast and a big plate of scrambled eggs. Hazel took stock.

"We can each have 2 and a half pieces of toast, but you can have as many eggs as you want."

### Page 7 [ ]

~~Ruby grabbed 3 pieces of toast, and no eggs.~~ (shown, not written)

"No!", Hazel reprimanded. "Weren't you listening Ruby? You didn't even get any eggs!"

"But I don't like ~~eggs~~", complained Ruby. *Does Ruby like eggs? Is there another food I could use instead?*

"But there's a huge plate, and there's exactly 5 pieces of toast, and-"

~~Ruby sighed, stuffed a piece of toast in her mouth and ran upstairs to pack.~~

Ruby stuck out her tongue and ran upstairs before Hazel could finish.

### Preparations

#### Page 8 [x]

Back upstairs, Hazel pulled out a scratchy cloth bag for their supplies. She put in:

 - a warm, yellow scarf with frills on the end
 - two apples, in case she or Ruby got hungry, and
 - an extra pair of purple gloves

Ruby packed a pair of sunglasses.

Hazel didn't say anything.

#### Page 9 [x]

But she did think it. Just a little.

## Into the Woods

### The sledding path

#### Page 10 [ ]

When they got to the park, Ruby and Hazel dragged their sled over to the edge and stood at the top of the big, big hill, looking down.

The trail wandered back and forth as if it couldn't quite make up its mind which direction was the best way to go. At the bottom lay a muddy brier patch surrounding a thick grove of trees. The path twisted sharply to the right as if in a sudden fit of resolution. Wherever it did end up, it seemed it wouldn't be within those woods.

It was impossible for the two children not to feel a sense of exhilaration as their eyes traced out the sledding route.

#### Page 11 [ ]

"We'll go down together, " Hazel said. "You hang on to me tightly, and then when we get close to the bottom, we both need to lean to the right as hard as we can!"

Ruby nodded.

#### Page 12 and 13 [ ] (?)

~~The two clambered onto the sled, paused for a second to catch their breath, and then kicked off.~~ (shown, not written)

They started off slowly (the fluffy snow wasn't quite packed enough to really accelerate), but as the angle of their descent got steeper they sped up and up until the world was a blur around them.

"Now Ruby!" commanded Hazel, and the two children leaned right as hard as they could. The sled banked sharply and whipped around the corner like a bullet, moving along the path into a wide open field and eventually slowing to a stop on the level ground.

The girls looked at each other, half breathless and half giggles.

"We have got to do that again," said Hazel.

### The argument

#### Page 14 (?)

Hazel and Ruby trudged up the path back towards the top of the hill, pausing to catch their breaths near the brier patch. (timing is weird, brier patch is at the bottom)

Ruby stared into the forest. "It's scary", she remarked.

"I heard at school that there's a Medusa in there."

"What's a Medusa?" Ruby asked.

"A Medusa is a creature that lives inside woods like this, " Hazel explained. "If you ever look it in the eyes, you turn into stone!"

Ruby scrunched up her face. "That isn't real!"

"Of course it's real, I already said that I learned it at school!" Hazel corrected. "If a Medusa ever shows up, you have to close your eyes. But then you can't see, and that's when the Medusa **gets you!**"

"No, it's not real!" Ruby protested.

"Stop being difficult!"

#### Page ?

The two children climbed back onto the sled and pushed off. The trail was tightly packed now, and the sled picked up speed quickly.

Ruby felt the wind and snow whipping past, drowning out the world around her. The sled moved faster and faster, blurring into an indecipherable blob of white and blue.

"Now, Ruby!" Hazel yelled, "lean!"

But Ruby didn't lean.

### The accident

#### Page ?

The sled flew off of the trail and careened towards the brier patch. In an instant of panic Hazel grabbed at Ruby and the two of them tumbled over and over into the snow. They lay quietly for a few moments.

Hazel carefully got up and checked to make sure she wasn't hurt, then turned on Ruby. "Weren't you listening?" she said, angrily. 

#### Page ?

"I **told** you we had to lean at the same time, and I **told** you that if we didn't lean we wouldn't make the turn and then we'd go straight into the brier patch and get-"

"My hands are cold", Ruby interjected quietly. She was still sitting down in the snow, looking at her now reddened fingers with an unreadable expression.

Hazel sighed and reached into her bag.

"I knew I needed to bring an extra pair of gloves because you didn't pack **any**."

Ruby didn't say anything. But she did take the gloves.

Hazel turned to trumble back up the hill when suddenly she noticed something missing.

"Ruby, where's the sled?" she asked, her eyes already widening with realization.

*Ruby points into the forest*

## Lost Together

*Does there need to be a transition into the forest, or will it be obvious enough that they just enter it?* (It will be obvious)

*Pictures will probably make this obvious... I'm not sure what I can and can't get away with.*

The woods swallowed the two children up.

"OK Ruby, we're just going to grab the sled, and then we're going to turn right around."

Ruby plowed ahead, but Hazel grabbed her hand and pulled her back.

"The first thing to remember when you're in the woods is to not split up."

Hazel pointed through the trees. "That's the direction we came. Make sure you remember it. That way, even if I get turned around, between the two of us we'll be able to figure it out."

The two plowed on.

"It's really quiet." Hazel remarked nervously. "Should woods be this quiet?"

Ruby didn't respond. The two moved deeper into the trees.

"I thought the sled would have gone more to the right," Hazel remarked nervously. "I don't know if we should go any farther than this."

Ruby wrestled her hand out of Hazel's and poked her head into a bush to see if anything was hiding behind it.

"This was a bad idea. I'm sure it didn't come this deep. Maybe it got caught under the brambles and we just walked past it."

"Hazel, look!"

There was a strange, dark shape behind the bush. Hazel and Ruby peered forward, squinting until they could make it out in the shadows. It was a badger, sitting silently, unmoving - like no animal they had ever seen before.

It was made of stone.

Hazel acted. "Ruby, close your eyes **right now**!"

Hazel slapped her hand over Ruby's eyes and the two girls were plunged together into darkness.

## A Confrontation

Hazel gripped Ruby's arm tightly and stumbled through the forest, tripping over rocks and branches.

"Do you remember which direction we came?"

Ruby shook her head, then remembered that Hazel couldn't see her. "No!"

A wave of doubt rushed over Hazel, but she shook it aside and kept her voice calm.

"It's OK Ruby. I am pretty sure I do. We just need to-"

Beside them, a branch snapped. Something was moving. It was too big to be a squirrel or a bird.

It was moving closer.

"You shouldn't be here", the something said.

Hazel pulled Ruby away, half sprinting through the thick underbrush.

"Wait!" The voice called behind them, frantically. Hazel could here it running behind them. It was keeping pace.

Her lungs felt like they were about to burst, but she was certain that this was the right direction. If she and Ruby could just move a little bit faster, in just a few more seconds they'd find themselves wading into soft snow, safe and sound, and everything would be-

Hazel tripped over something hard. It was covered with odd, unnatural lumps and sharp corners. Her hand felt around the cold surface, and with horror she realized that it was a stone badger.

They had been running in circles.

### Doubts

Hazel wondered what it would be like to be made of stone. Would she be stuck here, forever?

Would she be able to go to school? Hazel imagined herself sitting alone, in the woods, for days and weeks and months, waiting for something to happen. She clutched Ruby's hand tighter.

But Ruby's hand wasn't there.

You see, Ruby had been thinking too, about everything that Hazel had told her - about everything she knew.

She thought about that morning, and the way she felt when she was packing. She thought about whooshing down the hill, and the strange, almost purposeful way the path curved sharply to the right. She thought about the argument that she and Hazel had shared, and about a creature that could turn you to stone. She thought about a badger, running in circles, until it finally opened up its eyes in desperation. And she thought very hard about exactly where that badger was. And where she was.

And suddenly, something clicked. And Ruby decided to do something very brave.

She slipped her hand away from Hazel's and blindly felt along the snowy ground-

-until she felt the rough cloth surface of her bag

-and felt within the bag until her fingers closed around a small, solid object

-and stumbled across the snowy ground until she bumped into a strange, warm object

-and reached up until she felt its face

*Ruby puts the glasses on Medusa*

"Oh", said the Medusa.

Ruby opened her eyes. "Hello", she said.

### And Resolution

Ruby took stock. *How old is the Medusa? Adult or their age?* (Their age)

The creature stood awkwardly, like she wasn't sure what to do or how to respond. Hazel ran over, shaking a little, and grabbed Ruby's arm.

"I'm Ruby, and this is Hazel." Ruby tried again, holding out her hand.

Hesitantly, the Medusa reached out and shook her hand.

"You're a Medusa, right?"

"A gorgon, actually," the creature replied sheepishly. "Medusa's just my name."

"That's a weird name," Ruby offered cheerfully.

"Don't be rude, Ruby!" Hazel reprimanded. "Do you live in here?"

"Yeah. For as long as I can remember."

*This transition seems awkward. She goes from a threat to someone sympathetic to a friend in 3 paragraphs*

"I saw where your sled went," Medusa pointed. "That's why you're in here, right?"

Ruby and Hazel nodded.

"Nobody ever comes in here anymore. None of the animals do either, because whenever I try to play with them I turn them into stone."

She looked around miserably at the forest around her, littered with statues. "I can tell that they all hate me."

Hazel's instincts took over. "Actually, all you have to do is wear those sunglasses. You won't turn anyone into stone then. And anybody can come into the forest and play with you!"

"I can keep them?" Medusa beamed.

"Of course!" She paused, "I mean, I guess they're technically Ruby's. But she can have them, right Ruby?"

Ruby nodded.

They reached the edge of the forest and the gorgan stopped. Hazel wondered if she'd ever been out this far before.

A fox ran past and the Medusa instinctively raised a hand in front of her face, then cautiously lowered it. She waved to the fox.

"Thank you."

## Epilogue

The day was over, and the setting sun cast a deep purple shadow over the once-white blanket of snow.

As the first of the stars began to twinkle into existence, the two girls climbed into bed, both thinking about their adventures.

*Do Ruby and Hazel have bunk beds? I don't remember. Does it matter?*

After a while, Hazel spoke.

### Apologies

"Hey, Ruby? I'm glad you brought the sunglasses. And I'm glad that you were there when we got lost, you were really clever. I didn't know what to do."

"Thank you for the gloves, and for taking care of me in the forest. I'm sorry I wrecked the sled."

"It's OK, Ruby. I mean, it turned out OK, right?"

The children sat in silence for a few minutes.

"Ruby?"

"Yeah?"

"If I was turned into stone, you'd come visit me right? I mean... I was thinking about the animals, and if you hadn't brought the sunglasses, or if I had closed my eyes just a second later, or..." she trailed off.

Ruby flipped herself out of bed, flipped on a lamp, and walked over to Hazel's stack of books. She pulled down one that Hazel hadn't finished yet.

And Hazel laughed. "That's a great idea! We can go down tomorrow and we can read to them! And we can get their other animal friends to visit too since now the Medusa won't turn them into stone! And none of them will have to be lonely!"

Ruby climbed back into bed and slipped the book beneath her pillow, and the two children closed their eyes. The snow outside the house drifted down lazily, blanketing the world once again.

Now, of course, it was too far away for the children to really, honestly make out-

-but as they drifted off to sleep, both of them, just for a second, could swear that they heard an owl hooting from within a deep, dark forest.

## Final Graphic

*Hazel reading to the stone animals, Medusa, and Ruby*

